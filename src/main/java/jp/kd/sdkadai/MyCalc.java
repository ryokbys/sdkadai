package jp.kd.sdkadai;

import java.math.BigInteger;

public class MyCalc {
	private Integer num = 0;

	public Integer getNum(){
		return this.num;
	}

	public void setNum(Integer sn){
		this.num=sn;
	}

	public int ex1(Integer e1){
		return Math.abs(e1) * 5;
	}

	public boolean ex2(){
		int n = getNum();
		if(n<=1){
			return false;
		}
		for(int i=2;i<n;i++){
			if(n%i==0){
				return false;
			}
		}
		return true;
	}

	public String ex3(Integer e3){
		BigInteger bnum = new BigInteger(e3.toString());
		bnum=bnum.pow(e3);
		return bnum.toString();
	}
}
